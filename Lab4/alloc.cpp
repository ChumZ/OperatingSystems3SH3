/*The ﬁle alloc.cpp implements a resource allocator program. The allocator opens res.txt and maps it to a memory region using the system call mmap(). In a loop, it keeps asking how many units of a resource type is needed. Once entered, it subtracts the units from that resource type (if available) and then invokes system call msync() to synchronize the content of the mapped ﬁle with the physical ﬁle*/

#include <iostream>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/types.h> 
#include <sys/mman.h> 

using namespace std; 

int main(void){
	//Variable Decleration 
	int running = 1;
	char yn; 
	char type, amount; 
	int iamount, idata, testval;
	
	int fd, numType; 
	char *data; 

	//Open res.txt and map to memory
	if ((fd = open("res.txt", O_RDWR)) == -1){
		perror("open");
		exit(1);
	} 

	numType = 3; 
	data = (char*) mmap(0, numType*4, PROT_READ|PROT_WRITE, MAP_SHARED,  fd, 0); 
	if (data == MAP_FAILED) {
                perror ("mmap");
                return 1;
	}

	// 
	while(running){
		cout << "Do you wish to allocate resources(y/n)? \n";
		cin >> yn;

		if (yn == 'y' || yn == 'Y') {
			cout << "Enter Resource Type and Amount Needed: \n"; 
			cin >> type >> amount; 
			
			for (int i = 0; i < numType*4; i++){
				if(data[i] == type && (i%4 == 0)){
					idata = (int)data[i+2] - 48;
					iamount = (int)amount - 48; 
					testval = idata - iamount; 

					if (testval < 0) {
						cout << "Not Enough Resources" << "\n";
					} else {
						data[i+2] = (char) (testval+48); 
						msync(data,numType*4,MS_SYNC);  //Synchronize map file to physical file
					}
				}
			} 
		} else { 
			running = 0; 
		}
	}
	return 0; 
} 
