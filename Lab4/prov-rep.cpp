/* The parent process opens the ﬁle res.txt and maps it to a memory region (before forking the child process). The parent process also acts as a provider of resources. In a loop, it keeps asking whether new resources need to be added. If yes, it receives from the input the resource type and number of units and adds them to the memory region. Once added, using the system call msync(), it synchronize the content of the memory region with the physical ﬁle.  The child process is a reporter and reports three things every 10 seconds: ∗ The page size of the system using the system call getpagesize(). ∗ The current state of resources. ∗ The current status of pages in the memory region using the system call mincore() (i.e., whether pages of the calling process’s virtual memory are resident in core (RAM), and so will not cause a disk access (page fault) if referenced.)*/

#include <iostream>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/types.h> 
#include <sys/mman.h>
#include<signal.h>
#include <sys/stat.h> //

using namespace std; 

//Global Variables 
int running = 1; 

int main (void){
	//Variable Decleration 
	pid_t pid; 
	int fd; 
	char yn, type, amount;
	int iamount, idata, testval;

	//Open res.txt and map to memory
	if ((fd = open("res.txt", O_RDWR)) == -1){
		perror("open");
		exit(1);
	} 

	int numType = 3; 
	char* data = (char*) mmap(0, numType*4, PROT_READ|PROT_WRITE, MAP_SHARED,  fd, 0); 
	if (data == MAP_FAILED) {
                perror ("mmap");
                return 1;
	}

	if((pid = fork()) == -1){ 
		perror("fork");
		exit(1);
	}; 

	if (pid == 0){ //Child Process 
		int pageSize = getpagesize();
		int numPages = (numType*4 + pageSize -1)/ pageSize; 
		unsigned char vec[10]; 

		if(mincore(data, numType*4, &vec[0]) == -1){       /**/ 
			perror("mincore");
		} 

		while(running){
			cout << "\nREPORT: \n" << "Page Size is " << pageSize << "\n \n" << "State of Resources \n";
			for (int i = 0; i < numType*4; i++){
				cout << data[i];
			}
			cout << "\nStatus of Pages:\n"; 
	
			for (int j = 0; j < numPages; j++){ 		
				if ((vec[j] & 1) == 1){
					cout << "Page " << j << ": Resident \n" ;  
				} else { 
	 				cout << "Page " << j << ": Virtual \n" ;
				} 
			} 
			sleep(10); 
		} 	
		return 0; 
	} else {  //Parent Process
		while(running){
			cout <<  "\nAre Resources To Be Needed?(y/n): \n" ;
			cin >> yn; 

			if (yn == 'y' || yn == 'Y'){
				cout << "Enter Resource Type and Amount Needed: \n"; 
				cin >> type >> amount; 

				for (int i = 0; i < numType*4; i++){
					if(data[i] == type && (i%4 == 0)){
						idata = (int)data[i+2] - 48;
						iamount = (int)amount - 48; 
						testval = idata + iamount; 
	
						if (testval > 9) {
							testval = 9; 
							data[i+2] = (char) (testval+48); 
							msync(data,numType*4,MS_SYNC);  //Synchronize map file to physical file
						} else {
							data[i+2] = (char) (testval+48); 
							msync(data,numType*4,MS_SYNC);  //Synchronize map file to physical file
						}
					}
				} 
			} else { 
				running = 0; 
			} 
		} 
		return 0; 
	} 
} 
