#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/types.h> 
#include <string.h> 

int flag = 1; 

int main(void){
	pid_t pid; 
	int p2c[2], c2p[2];  //file descriptors 
	char numIn[1]; 
	int arr[10];
	int sum = 0; 
	int count =0; 

	pipe(p2c); 
	pipe(c2p); 

	pid = fork(); 

	if(pid == -1){ 
		perror("fork");
		exit(1);
	}; 

	if(pid == 0){ //Child Process
		close(c2p[0]);  
		close(p2c[1]); 
		
		while(flag == 1){
			printf("Enter Integer: "); 
			scanf("%d", numIn); 
			write(c2p[1], numIn, 1); 
			if ((int)numIn[0] == -1) {flag = 0;}; 
		};

		read(p2c[0], &sum, 4);  
		printf("The Result is %d\n", sum); 

		return 0; 
	} else {  //  Parent 
		close(c2p[1]);
		close(p2c[0]); 

		while(flag == 1){
			read(c2p[0], numIn, 1); 
			count++;  
			arr[count-1] = (int)numIn[0]; 
			if (arr[count-1] == -1) {flag = 0;}; 
		};

		for(int i=0; i<count;i++){
			sum = sum + arr[i]; 
		}; 

		write(p2c[1], &sum, 4); 
		
		return 0; 
	}; 
}
