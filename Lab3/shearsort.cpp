//Lab 3: POISX Threads by Nicholas Chumney 
#include <iostream>
#include <fstream> 
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pthread.h>
#include <semaphore.h>
#include <math.h>

using namespace std; 

#define NUM_INT 16		//must be perfect square 
#define NUM_ROW 4		//sqrt(NUM_INT) 

//Global Variables 
int myArray [NUM_ROW][NUM_ROW]; 
int N = 0; 			//Number Threads running
long numPhase = (long) floor(log2(NUM_INT) + 1);
sem_t mutex, halt,halt1,halt2,halt3;
void* status; 

//Functions 
void initArray(){
	ifstream file("input.txt"); 
	if(file.is_open()){ 
		for(int m = 0; m < NUM_ROW; m++){
			for (int n = 0; n < NUM_ROW; n++){
				file >> myArray[m][n]; 
			}
		}
	} 
}
void printArray() { 
	for (int m = 0; m < NUM_ROW; m++){
		for(int n = 0; n < NUM_ROW; n++){ 
			cout << myArray[m][n] << " "; 
		}
		printf("\n"); 
	} 
	printf("\n"); 
} 
void bubbleSort (int sortArr[]){ 
	bool swapped = true; 
	int temp; 
	int j = 0; 
	while(swapped){ 
		swapped = false; 
		j++;
		for (int i = 0; i < NUM_ROW - j; i++){
			if (sortArr[i] > sortArr[i+1]){
				temp = sortArr[i+1];
				sortArr[i+1] = sortArr[i]; 
				sortArr[i] = temp; 
				swapped = true; 
			} 
		} 
	}
}

void bubbleSortR (int sortArr[]){ 
	bool swapped = true; 
	int temp; 
	int j = 0; 
	while(swapped){ 
		swapped = false; 
		j++;
		for (int i = 0; i < NUM_ROW - j; i++){
			if (sortArr[i] < sortArr[i+1]){
				temp = sortArr[i+1];
				sortArr[i+1] = sortArr[i]; 
				sortArr[i] = temp; 
				swapped = true; 
			} 
		} 
	}
}

void *sort(void *arg){
	int localArray[NUM_ROW]; 
	long rc = (long) arg;
	int phasecount = 0; 

	while(phasecount < numPhase){ 		
		//Pull for global Array
		sem_wait(&mutex);
		if (phasecount%2 == 0){
			for (int i = 0; i < NUM_ROW; i++){ 
				localArray[i] = myArray[rc][i];
			}
		} else { 
			for (int i = 0; i < NUM_ROW; i++){ 
				localArray[i] = myArray[i][rc];
			}
		} 
		sem_post(&mutex); 

		//Sort 
		if(rc%2 != 0 && phasecount%2 == 0 ) {
			bubbleSortR(localArray); 
		} else {
			bubbleSort(localArray); 
		}
	
		//update global array 
		sem_wait(&mutex); 
		if (phasecount%2 == 0){
			for (int i = 0; i < NUM_ROW; i++){ 
				myArray[rc][i] = localArray[i];
			}
		} else { 
			for (int i = 0; i < NUM_ROW; i++){ 
				myArray[i][rc] = localArray[i];
			} 
		} 	
		N++; 
		sem_post(&mutex); 

		if (N%NUM_ROW != 0 && phasecount == 0) {sem_wait(&halt);}
		if(phasecount == 0) sem_post(&halt); 

		if (N%NUM_ROW != 0 && phasecount == 1) {sem_wait(&halt1);}
		if(phasecount == 1) sem_post(&halt1); 

		if (N%NUM_ROW != 0 && phasecount == 2) {sem_wait(&halt2);}
		if(phasecount == 2) sem_post(&halt2); 

		if (N%NUM_ROW != 0 && phasecount == 3) {sem_wait(&halt3);}
		if(phasecount == 3) sem_post(&halt3); 

		//Go to Next Phase
		phasecount++;
	} 
	pthread_exit(NULL); 
}

//MAIN
int main (void) {
	//Variable Declerartion
	pthread_t thread[NUM_ROW];
	pthread_attr_t attr;
	int err; 

	//Initialize Array 
	initArray();
	printArray(); 

	//Initialize Semaphores
	sem_init(&mutex, 0, 1);
	sem_init(&halt, 0, 0);
	sem_init(&halt1, 0, 0);
	sem_init(&halt2, 0, 0);
	sem_init(&halt3, 0, 0);


	//Create Threads 
	pthread_attr_init(&attr);
  	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for(int i =0;i < NUM_ROW; i++){ 
		err = pthread_create(&thread[i], &attr, sort,(void*) i);
		if (err){
			printf("Error");
			exit(-1);
		}
	} 
	pthread_attr_destroy(&attr);
	
	//Wait on threads 
	for(int t=0; t<NUM_ROW; t++) {
      		pthread_join(thread[t], &status);
        }

	//Print Sorted Array 
	printArray(); 

	sem_destroy(&mutex);
	sem_destroy(&halt); 
	pthread_exit(NULL); 
	return 0; 
} 
