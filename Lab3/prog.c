#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <semaphore.h>
#include <math.h>

#define N	4

FILE *fr;
int i,j,k,l,m,holder,rc;
int count = 0;
int count2 = 0;
int count3 = 0;
int phase = 0;
long t;
int mat [N][N];
sem_t mutex,wait,wait2;

void *Sort(void *threadid){
	int maxphase = 5;

	while(phase < maxphase){
	sem_wait(&mutex);
	long tid;
	tid = (long)threadid;

	if(tid%2 == 0){
		for (j = 0; j <= N-1; j++){
			for (i=0; i <  N-1; i++){
				if(mat[tid][i] > mat[tid][i+1]){
					holder = mat[tid][i];
					mat[tid][i] = mat[tid][i+1];
					mat[tid][i+1] = holder;
	
				}
			}
		}
	}

	else if(tid%2 != 0){
		for (j = 0; j <= N-1; j++){
			for (i=0; i <  N-1; i++){
				if(mat[tid][i] < mat[tid][i+1]){
					holder = mat[tid][i];
					mat[tid][i] = mat[tid][i+1];
					mat[tid][i+1] = holder;
	
				}
			}
		}

	}
	count2++;
	if(count2 != N){
		sem_post(&mutex);
		sem_wait(&wait);

	}
	count2 = 0;
	tid = (long)threadid;
	
	for (k = 0; k <= N-1; k++){
		for (l=0; l <  N-1; l++){
			if(mat[l][tid] > mat[l+1][tid]){
				holder = mat[l][tid];
				mat[l][tid] = mat[l+1][tid];
				mat[l+1][tid] = holder;
			}
		}
	}
	count3++;
	if(count3 != N){
		sem_post(&wait);
		sem_wait(&wait2);
		sem_wait(&mutex);
	}

	if(count3 == N){
		for(count3 = N; count3 > 0; count3--){
			sem_post(&wait2);
		} 
		phase = phase + 1;

	}
	sem_post(&mutex);

	}

	count ++;
	if(count == N){
	printf("\n");
		for (i = 0; i < N; i++){
			for(j = 0; j < N; j++){
				printf("%d ", mat[i][j]);
			}
			printf("\n");
		}
	}

	pthread_exit(NULL);
	


}

int main(void){

	fr = fopen("input.txt", "r");
	for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			fscanf(fr, "%d", &mat[i][j]);
		}
	}
	
	for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			printf("%d ", mat[i][j]);
		}
		printf("\n");
	}

	sem_init(&mutex, 0, 1);
	sem_init(&wait, 0, 0);
	sem_init(&wait2, 0, 0);


	pthread_t threads[N];

	for(t=0; t < N ; t++){
		rc = pthread_create(&threads[t], NULL, Sort, (void *)t);

		if (rc){
			printf("Error");
			exit(-1);
		}
	}
	

	pthread_exit(NULL);
}
