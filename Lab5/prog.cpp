// This program has two functions: 1) Report File Attributs 
//				   2) Traverse Directories 

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h> 
#include <iostream> 
#include <dirent.h> 
#include <time.h> 
#include <pwd.h> 
#include <grp.h>
#include <math.h> 

using namespace std; 

////// Function //////////////////////////////////////////////////////////////
void  Traverse(const char* dirname){
	//Variable Decleration
	unsigned char isDir = 0x4; 
	DIR* dirp; 
	struct dirent *dp; 
	char* cwd;
	struct stat fd; 

	if (stat(dirname, &fd) == -1) { 
		perror ("stat");
		exit(EXIT_FAILURE);
	} 

	//Get Current Directory 
	if ((cwd = getcwd(NULL, 64)) == NULL) {
        	perror("pwd");
       		return; 
   	}
	
	//Check Permission to read 
	if(!(fd.st_mode & S_IRUSR)){ 
		perror("No Permission to read");
		exit;
	} 

	//Open Selected Directory
	if((dirp = opendir(dirname)) == NULL) { 
		perror("Cannot Open");
		return;
	} 

	//Read Through Directory
	while(dp = readdir(dirp)){ 
		cout << dp->d_name << '\n' ;

		if(dp->d_type == isDir){ 
			if(!(!strcmp(dp->d_name, ".") || !strcmp(dp->d_name,".."))){
				if((chdir(dirname)) == -1){
					perror("chdir");
					return;
				} 
				Traverse(dp->d_name) ; 
				chdir(cwd); 
			}
		} 
	} 

	//Close Selected Directory 
	closedir(dirp); 
}
////// Main //////////////////////////////////////////////////////////////////
int main(int argc, char* argv[]) { 
	//Check if Argument was given 
	if (argc != 2) {
		cerr << "Usage:./prog (NAME) \n" ; 
	}

	//Variable Decleration 
	struct stat fd; 
	struct passwd *pwd; 
	struct group *grp; 

	if (stat(argv[1], &fd) == -1) { 
		perror ("stat");
		exit(EXIT_FAILURE);
	} 

	if (fd.st_mode & S_IFDIR){ 
		cout << "Regular Directory \n\n" ; 
		Traverse(argv[1]); 
	} else {		
		//Get Usersname and group name 
		pwd = getpwuid(fd.st_uid);
		grp = getgrgid(fd.st_gid);
		cout << "Regular File \n\n" ;

		cout << "Name: " << argv[1] << '\n'; 
		cout << "Mode: " << oct << fd.st_mode << '\n'; // Need Octal
		cout << "Number of Links: " << fd.st_nlink << '\n';  
		cout << "Owner's Name: " << pwd->pw_name  << '\n'; 
		cout << "Group Name: " << grp->gr_name << '\n'; 
		cout << "Size (Bytes): " << fd.st_size << '\n';
		cout << "Size (Blocks): " << ceil(fd.st_size/4096.0) << '\n'; 
		cout << "Last Modification Time: " << ctime(&fd.st_mtime);
	}
	
	return 0;
}
